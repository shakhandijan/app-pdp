import React, {Component} from 'react';
import logo from '../images/pdp-xl-logo.svg'
import HomePage from "./HomePage";
import './MenuPage.css'
import 'bootstrap/dist/css/bootstrap.min.css'
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import {faCog, faArrowRight, faExternalLinkAlt, faPowerOff} from "@fortawesome/free-solid-svg-icons";
import {faDatabase} from "@fortawesome/free-solid-svg-icons";


class MenuPage extends Component {
    render() {
        return (
            <div className="container ">
                <div className="header">
                    <a className="logo">
                        <img src={logo} alt=""/>
                        <a href={HomePage}>PDP <br/>IT-ACADEMY</a>
                    </a>
                </div>
                <div className="menu_body">
                    <div className="form-group dashboard">
                        <a href="#">
                            <FontAwesomeIcon icon={faDatabase} style={{margin: "0px 20px 0 0"}}/>
                            Dashboard
                        </a>
                    </div>
                    <div className="form-group cog">
                        <a href="#">
                            <FontAwesomeIcon icon={faCog} style={{margin: "0px 20px 0 0"}}/>
                            Sozlamalar
                        </a>
                    </div>
                </div>
                <div className="menu_footer">
                    <div className="form-group">
                        <a href="#">
                            <FontAwesomeIcon icon={faExternalLinkAlt} style={{margin: "0px 20px 0 0"}}/>
                            Home <span>pdp.uz</span>
                        </a>
                    </div>
                    <div className="form-group">
                        <a href="#">
                            <FontAwesomeIcon icon={faPowerOff} style={{margin: "0px 20px 0 0"}}/>
                            Chiqish
                        </a>
                    </div>
                </div>
            </div>
        );
    }
}

export default MenuPage;