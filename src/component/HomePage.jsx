import React, {Component} from 'react';
import 'bootstrap/dist/css/bootstrap.min.css'
import MenuPage from "./MenuPage";
import './HomePage.css'
import Dashboard from "./Dashboard";

class HomePage extends Component {
    render() {
        return (
            <div className="container">
                <div className="row">
                    <div className="col-md-2 menu">
                        <MenuPage/>
                    </div>
                    <div className="col-md-9">
                        <Dashboard/>
                    </div>
                </div>
            </div>
        );
    }
}

export default HomePage;